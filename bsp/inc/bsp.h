/*
 * bsp.h
 *
 *  Created on: 5 ao�t 2017
 *      Author: Laurent
 */

#ifndef BSP_INC_BSP_H_
#define BSP_INC_BSP_H_

#include "stm32f0xx.h"

/*
 * Debug Console init
 */

void	BSP_Console_Init	(void);

/*
 * Timer functions
 */

void BSP_TIMER_Timebase_Init	(void);
void BSP_TIMER_IC_Init			(void);

void 		BSP_INPUTS_Init			(void);
uint32_t 	BSP_GET_INPUTS			(void);

#endif /* BSP_INC_BSP_H_ */
