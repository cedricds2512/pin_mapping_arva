/*
 * delay.h
 *
 *  Created on: 6 ao�t 2017
 *      Author: Laurent
 */

#ifndef BSP_INC_DELAY_H_
#define BSP_INC_DELAY_H_

#include "stm32f0xx.h"


/*
 * Timer delays
 */

void BSP_DELAY_TIM_init		(void);
void BSP_DELAY_TIM_ms		(uint16_t ms);

#endif /* BSP_INC_DELAY_H_ */
