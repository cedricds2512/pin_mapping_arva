/*
 * bsp.c
 *
 *  Created on: 5 ao�t 2017
 *      Author: Laurent
 */

#include "bsp.h"


/*********************************************************************************************************/
/*
 * BSP_Console_Init()
 * USART2 @ 115200 Full Duplex
 * 1 start - 8-bit - 1 stop
 * TX -> PA2 (AF1)
 * RX -> PA3 (AF1)
 */

void BSP_Console_Init()
{
	// Enable GPIOA clock
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;

	// Configure PA2 and PA3 as Alternate function
	GPIOA->MODER &= ~(GPIO_MODER_MODER2_Msk | GPIO_MODER_MODER3_Msk);
	GPIOA->MODER |= (0x02 <<GPIO_MODER_MODER2_Pos) | (0x02 <<GPIO_MODER_MODER3_Pos);

	// Set PA2 and PA3 to AF1 (USART2)
	GPIOA->AFR[0] &= ~(0x0000FF00);
	GPIOA->AFR[0] |=  (0x00001100);

	// Enable USART2 clock
	RCC -> APB1ENR |= RCC_APB1ENR_USART2EN;

	// Clear USART2 configuration (reset state)
	// 8-bit, 1 start, 1 stop, CTS/RTS disabled
	USART2->CR1 = 0x00000000;
	USART2->CR2 = 0x00000000;
	USART2->CR3 = 0x00000000;

	// Select PCLK (APB1) as clock source
	// PCLK -> 48 MHz
	RCC->CFGR3 &= ~RCC_CFGR3_USART2SW_Msk;

	// Baud Rate = 115200
	// With OVER8=0 and Fck=48MHz, USARTDIV =   48E6/115200 = 416.6666
	// BRR = 417 -> Baud Rate = 115107.9137 -> 0.08% error
	//
	// With OVER8=1 and Fck=48MHz, USARTDIV = 2*48E6/115200 = 833.3333
	// BRR = 833 -> Baud Rate = 115246.0984 -> 0.04% error (better)

	USART2->CR1 |= USART_CR1_OVER8;
	USART2->BRR = 833;

	// Enable both Transmitter and Receiver
	USART2->CR1 |= USART_CR1_TE | USART_CR1_RE;

	// Enable USART2
	USART2->CR1 |= USART_CR1_UE;
}

void BSP_TIMER_Timebase_Init()
{
	// Enable TIM6 clock
	RCC->APB1ENR |= RCC_APB1ENR_TIM6EN;

	// Reset TIM6 configuration
	TIM6->CR1 = 0x0000;
	TIM6->CR2 = 0x0000;

	// Set TIM6 prescaler
	// Fck = 48MHz -> /48 = 1MHz counting frequency
	TIM6->PSC = (uint16_t) 48 -1;

	// Set TIM6 auto-reload register for 200�s period
	TIM6->ARR = (uint16_t) 200 -1;

	// Enable auto-reload preload
	TIM6->CR1 |= TIM_CR1_ARPE;

	// Start TIM6 counter
	TIM6->CR1 |= TIM_CR1_CEN;
}

/*
 * BSP_TIMER_IC_Init()
 * TIM3 as Input Capture
 * Channel 1 -> PB4 (AF1)
 */

void BSP_TIMER_IC_Init()
{
	// Enable GPIOB clock
	RCC->AHBENR |= RCC_AHBENR_GPIOBEN;

	// Configure PB4 as Alternate function
	GPIOB->MODER &= ~(GPIO_MODER_MODER4_Msk);
	GPIOB->MODER |=  (0x02 <<GPIO_MODER_MODER4_Pos);

	// Set PB4 to AF1 (TIM3_CH1)
	GPIOB->AFR[0] &= ~(0x000F0000);
	GPIOB->AFR[0] |=  (0x00010000);

	// Enable TIM3 clock
	RCC -> APB1ENR |= RCC_APB1ENR_TIM3EN;

	// Reset TIM3 configuration
	TIM3->CR1  = 0x0000;
	TIM3->CR2  = 0x0000;
	TIM3->CCER = 0x0000;

	// Set TIM3 prescaler
	// Fck = 48MHz -> /48000 = 1KHz counting frequency
	TIM3->PSC = (uint16_t) 48000 -1;

	// Set Auto-Reload to maximum value
	TIM3->ARR = (uint16_t) 0xFFFF;

	// Reset Input Capture configuration
	TIM3->CCMR1 = 0x0000;
	TIM3->CCMR2 = 0x0000;

	// Channel 1 input on TI1
	TIM3->CCMR1 |= (0x01 <<TIM_CCMR1_CC1S_Pos);

	// Channel 2 input also on TI1
	TIM3->CCMR1 |= (0x02 <<TIM_CCMR1_CC2S_Pos);

	// Filter both channels with N=8
	TIM3->CCMR1 |= (0x03 <<TIM_CCMR1_IC1F_Pos) | (0x03 <<TIM_CCMR1_IC2F_Pos);

	// Select falling edge for channel 1
	TIM3->CCER |= (0x00 <<TIM_CCER_CC1NP_Pos) | (0x01 <<TIM_CCER_CC1P_Pos);

	// Select rising edge for channel 2
	TIM3->CCER |= (0x00 <<TIM_CCER_CC2NP_Pos) | (0x00 <<TIM_CCER_CC2P_Pos);

	// Enable capture on channel 1 & channel 2
	TIM3->CCER |= (0x01 <<TIM_CCER_CC1E_Pos) | (0x01 <<TIM_CCER_CC2E_Pos);

	// Choose Channel 1 as trigger input
	TIM3->SMCR |= (0x05 <<TIM_SMCR_TS_Pos);

	// Slave mode -> Resets counter when trigger occurs
	TIM3->SMCR |= (0x4 <<TIM_SMCR_SMS_Pos);

	// Enable TIM3
	TIM3->CR1 |= TIM_CR1_CEN;
}

/*
 * BSP_INPUTS_Init()
 * Initializes all necesarry pins for our pinmapping (in port A and C) as input with pull down resistor
 */

void BSP_INPUTS_Init()
{
	// Enable GPIOA & GPIOC clock
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
	RCC->AHBENR |= RCC_AHBENR_GPIOCEN;

	// Configure pins as input -> value = 00
		//PORTA
	GPIOA->MODER &= ~GPIO_MODER_MODER0_Msk;		//PA0
	GPIOA->MODER &= ~GPIO_MODER_MODER1_Msk;		//PA1
	GPIOA->MODER &= ~GPIO_MODER_MODER4_Msk;		//PA4
	GPIOA->MODER &= ~GPIO_MODER_MODER5_Msk;		//PA5
	GPIOA->MODER &= ~GPIO_MODER_MODER6_Msk;		//PA6
	GPIOA->MODER &= ~GPIO_MODER_MODER7_Msk;		//PA7
	GPIOA->MODER &= ~GPIO_MODER_MODER8_Msk;		//PA8
	GPIOA->MODER &= ~GPIO_MODER_MODER9_Msk;		//PA9
	GPIOA->MODER &= ~GPIO_MODER_MODER10_Msk;		//PA10		//added after soldering
		//PORTC
	GPIOC->MODER &= ~GPIO_MODER_MODER0_Msk;		//PC0
	GPIOC->MODER &= ~GPIO_MODER_MODER1_Msk;		//PC1
	GPIOC->MODER &= ~GPIO_MODER_MODER2_Msk;		//PC2
	GPIOC->MODER &= ~GPIO_MODER_MODER3_Msk;		//PC3
	GPIOC->MODER &= ~GPIO_MODER_MODER4_Msk;		//PC4
	GPIOC->MODER &= ~GPIO_MODER_MODER5_Msk;		//PC5
	GPIOC->MODER &= ~GPIO_MODER_MODER6_Msk;		//PC6
	GPIOC->MODER &= ~GPIO_MODER_MODER7_Msk;		//PC7
	//GPIOC->MODER &= ~GPIO_MODER_MODER8_Msk;		//PC8		//not soldered
	//GPIOC->MODER &= ~GPIO_MODER_MODER9_Msk;		//PC9
	GPIOC->MODER &= ~GPIO_MODER_MODER10_Msk;	//PC10
	GPIOC->MODER &= ~GPIO_MODER_MODER11_Msk;	//PC11
	GPIOC->MODER &= ~GPIO_MODER_MODER12_Msk;	//PC12

	// Configure PULL DOWN -> first 00 then value = 10
		//PORTA
	GPIOA->PUPDR &= ~GPIO_PUPDR_PUPDR0_Msk;		//PA0
	GPIOA->PUPDR &= ~GPIO_PUPDR_PUPDR1_Msk;		//PA1
	GPIOA->PUPDR &= ~GPIO_PUPDR_PUPDR4_Msk;		//PA4
	GPIOA->PUPDR &= ~GPIO_PUPDR_PUPDR5_Msk;		//PA5
	GPIOA->PUPDR &= ~GPIO_PUPDR_PUPDR6_Msk;		//PA6
	GPIOA->PUPDR &= ~GPIO_PUPDR_PUPDR7_Msk;		//PA7
	GPIOA->PUPDR &= ~GPIO_PUPDR_PUPDR8_Msk;		//PA8
	GPIOA->PUPDR &= ~GPIO_PUPDR_PUPDR9_Msk;		//PA9
	GPIOA->PUPDR &= ~GPIO_PUPDR_PUPDR10_Msk;		//PA10

	GPIOA->PUPDR |= GPIO_PUPDR_PUPDR0_1;		//PA0
	GPIOA->PUPDR |= GPIO_PUPDR_PUPDR1_1;		//PA1
	GPIOA->PUPDR |= GPIO_PUPDR_PUPDR4_1;		//PA4
	GPIOA->PUPDR |= GPIO_PUPDR_PUPDR5_1;		//PA5
	GPIOA->PUPDR |= GPIO_PUPDR_PUPDR6_1;		//PA6
	GPIOA->PUPDR |= GPIO_PUPDR_PUPDR7_1;		//PA7
	GPIOA->PUPDR |= GPIO_PUPDR_PUPDR8_1;		//PA8
	GPIOA->PUPDR |= GPIO_PUPDR_PUPDR9_1;		//PA9
	GPIOA->PUPDR |= GPIO_PUPDR_PUPDR10_1;		//PA10
		//PORTC
	GPIOC->PUPDR &= ~GPIO_PUPDR_PUPDR0_Msk;		//PC0
	GPIOC->PUPDR &= ~GPIO_PUPDR_PUPDR1_Msk;		//PC1
	GPIOC->PUPDR &= ~GPIO_PUPDR_PUPDR2_Msk;		//PC2
	GPIOC->PUPDR &= ~GPIO_PUPDR_PUPDR3_Msk;		//PC3
	GPIOC->PUPDR &= ~GPIO_PUPDR_PUPDR4_Msk;		//PC4
	GPIOC->PUPDR &= ~GPIO_PUPDR_PUPDR5_Msk;		//PC5
	GPIOC->PUPDR &= ~GPIO_PUPDR_PUPDR6_Msk;		//PC6
	GPIOC->PUPDR &= ~GPIO_PUPDR_PUPDR7_Msk;		//PC7
	//GPIOC->PUPDR &= ~GPIO_PUPDR_PUPDR8_Msk;		//PC8
	//GPIOC->PUPDR &= ~GPIO_PUPDR_PUPDR9_Msk;		//PC9
	GPIOC->PUPDR &= ~GPIO_PUPDR_PUPDR10_Msk;	//PC10
	GPIOC->PUPDR &= ~GPIO_PUPDR_PUPDR11_Msk;	//PC11
	GPIOC->PUPDR &= ~GPIO_PUPDR_PUPDR12_Msk;	//PC12

	GPIOC->PUPDR |= GPIO_PUPDR_PUPDR0_1;		//PC0
	GPIOC->PUPDR |= GPIO_PUPDR_PUPDR1_1;		//PC1
	GPIOC->PUPDR |= GPIO_PUPDR_PUPDR2_1;		//PC2
	GPIOC->PUPDR |= GPIO_PUPDR_PUPDR3_1;		//PC3
	GPIOC->PUPDR |= GPIO_PUPDR_PUPDR4_1;		//PC4
	GPIOC->PUPDR |= GPIO_PUPDR_PUPDR5_1;		//PC5
	GPIOC->PUPDR |= GPIO_PUPDR_PUPDR6_1;		//PC6
	GPIOC->PUPDR |= GPIO_PUPDR_PUPDR7_1;		//PC7
	//GPIOC->PUPDR |= GPIO_PUPDR_PUPDR8_1;		//PC8
	//GPIOC->PUPDR |= GPIO_PUPDR_PUPDR9_1;		//PC9
	GPIOC->PUPDR |= GPIO_PUPDR_PUPDR10_1;		//PC10
	GPIOC->PUPDR |= GPIO_PUPDR_PUPDR11_1;		//PC11
	GPIOC->PUPDR |= GPIO_PUPDR_PUPDR12_1;		//PC12

}

/*
 * BSP_GET_INPUTS()
 * output one 32 bit integer containing values from port A and C.
 */

uint32_t BSP_GET_INPUTS()
{
	uint32_t all_inputs;
	uint32_t measure[3];
	all_inputs = 0;

	uint32_t tempA;
	uint32_t tempC;

	for( uint8_t i = 0; i < 3; i++ )
	{
		BSP_DELAY_TIM_ms(35);

		tempA = (uint32_t) GPIOA->IDR;
		tempC = (uint32_t) GPIOC->IDR << 16;
		measure[i] = tempA | tempC;			//concatenate portA and portC
	}

	all_inputs |= measure[0];
	all_inputs |= measure[1];
	all_inputs |= measure[2];

	return all_inputs;
}
