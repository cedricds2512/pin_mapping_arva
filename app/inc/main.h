#ifndef MAIN_H
#define MAIN_H
/*
 * pin defines
 */
#define GPIO_A_0                      (0x00000001U)
#define GPIO_A_1                      (0x00000002U)
#define GPIO_A_2                      (0x00000004U)
#define GPIO_A_3                      (0x00000008U)
#define GPIO_A_4                      (0x00000010U)
#define GPIO_A_5                      (0x00000020U)
#define GPIO_A_6                      (0x00000040U)
#define GPIO_A_7                      (0x00000080U)
#define GPIO_A_8                      (0x00000100U)
#define GPIO_A_9                      (0x00000200U)
#define GPIO_A_10                     (0x00000400U)
#define GPIO_A_11                     (0x00000800U)
#define GPIO_A_12                     (0x00001000U)
#define GPIO_A_13                     (0x00002000U)
#define GPIO_A_14                     (0x00004000U)
#define GPIO_A_15                     (0x00008000U)

#define GPIO_C_0                      (0x00000001U << 16)
#define GPIO_C_1                      (0x00000002U << 16)
#define GPIO_C_2                      (0x00000004U << 16)
#define GPIO_C_3                      (0x00000008U << 16)
#define GPIO_C_4                      (0x00000010U << 16)
#define GPIO_C_5                      (0x00000020U << 16)
#define GPIO_C_6                      (0x00000040U << 16)
#define GPIO_C_7                      (0x00000080U << 16)
#define GPIO_C_8                      (0x00000100U << 16)
#define GPIO_C_9                      (0x00000200U << 16)
#define GPIO_C_10                     (0x00000400U << 16)
#define GPIO_C_11                     (0x00000800U << 16)
#define GPIO_C_12                     (0x00001000U << 16)
#define GPIO_C_13                     (0x00002000U << 16)
#define GPIO_C_14                     (0x00004000U << 16)
#define GPIO_C_15                     (0x00008000U << 16)

/*
 * number combinations
 */

#define D1_0		(GPIO_C_1 | GPIO_C_10 | GPIO_C_11 | GPIO_C_12 | GPIO_A_0 | GPIO_A_4)
#define D1_1		(GPIO_A_4 | GPIO_A_0)
#define D1_2		(GPIO_C_1 | GPIO_A_4 | GPIO_A_1 | GPIO_C_11 | GPIO_C_12)
#define D1_3		(GPIO_C_1 | GPIO_A_4 | GPIO_A_1 | GPIO_A_0 | GPIO_C_12)
#define D1_4		(GPIO_C_10 | GPIO_A_1 | GPIO_A_4 | GPIO_A_0)
#define D1_5		(GPIO_C_1 | GPIO_C_10 | GPIO_A_1 | GPIO_A_0 | GPIO_C_12)
#define D1_6		(GPIO_C_1 | GPIO_C_10 | GPIO_C_11 | GPIO_C_12 | GPIO_A_0 | GPIO_A_1)
#define D1_7		(GPIO_C_1 | GPIO_A_4 | GPIO_A_0)
#define D1_8		(GPIO_C_1 | GPIO_C_10 | GPIO_C_11 | GPIO_C_12 | GPIO_A_0 | GPIO_A_4 | GPIO_A_1)
#define D1_9		(GPIO_C_1 | GPIO_C_10 | GPIO_C_12 | GPIO_A_0 | GPIO_A_4 | GPIO_A_1)

#define D1_msk		(GPIO_C_1 | GPIO_C_10 | GPIO_C_11 | GPIO_C_12 | GPIO_A_0 | GPIO_A_4 | GPIO_A_1)

#define DOT			(GPIO_C_2)

#define D2_0		(GPIO_C_0 | GPIO_C_6 | GPIO_C_5 | GPIO_A_8 | GPIO_C_3 | GPIO_C_4)
#define D2_1		(GPIO_C_5 | GPIO_C_4)
#define D2_2		(GPIO_C_6 | GPIO_C_5 | GPIO_A_10 | GPIO_A_8 | GPIO_C_3)
#define D2_3		(GPIO_C_6 | GPIO_C_5 | GPIO_A_10 | GPIO_C_3 | GPIO_C_4)
#define D2_4		(GPIO_C_0 | GPIO_C_5 | GPIO_A_10 | GPIO_C_4)
#define D2_5		(GPIO_C_0 | GPIO_C_6 | GPIO_A_10 | GPIO_C_3 | GPIO_C_4)
#define D2_6		(GPIO_C_0 | GPIO_C_6 | GPIO_A_10 | GPIO_A_8 | GPIO_C_3 | GPIO_C_4)
#define D2_7		(GPIO_C_6 | GPIO_C_5 | GPIO_C_4)
#define D2_8		(GPIO_C_0 | GPIO_C_6 | GPIO_C_5 | GPIO_A_10 | GPIO_A_8 | GPIO_C_3 | GPIO_C_4)
#define D2_9		(GPIO_C_6 | GPIO_C_5 | GPIO_A_10 | GPIO_C_0 | GPIO_C_3 | GPIO_C_4)

#define D2_msk 		(GPIO_C_0 | GPIO_C_6 | GPIO_C_5 | GPIO_A_10 | GPIO_A_8 | GPIO_C_3 | GPIO_C_4)

#define ARR_1		(GPIO_A_9)
#define ARR_2		(GPIO_C_7)
#define ARR_3		(GPIO_A_7)
#define ARR_4		(GPIO_A_6)
#define ARR_5		(GPIO_A_5)
#endif /* MAIN_H */
