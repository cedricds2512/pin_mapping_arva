#include "main.h"
#include "stm32f0xx.h"
#include "delay.h"
#include "bsp.h"

/*
 * function prototypes
 */
static void SystemClock_Config(void);
uint8_t get_digit_1(uint32_t);
uint8_t get_digit_2(uint32_t);
uint8_t get_arrow(uint32_t);
uint8_t get_dot(uint32_t);

void test_pins(uint32_t input);


/*
 * main function
 */
int main()
{
	uint32_t input = 0;
	uint8_t output[4]; //digit1 ; digit2 ; arrow ; dot



	//configure clock
	SystemClock_Config();

	// Initialize Debug Console
	BSP_Console_Init();
		//my_printf("Console ready!\r\n");

	//init timer
	BSP_DELAY_TIM_init();
	//init inputs
	BSP_INPUTS_Init();


	while(1)
	{
		input = BSP_GET_INPUTS();

		if(get_digit_1(input) != 99) {output[0] = get_digit_1(input);}
		if(get_digit_2(input) != 99) {output[1] = get_digit_2(input);}
		if(get_arrow(input) != 0 || output[0]<3) {output[2] = get_arrow(input);}
		output[3] = get_dot(input);

		for(int i = 0; i<4 ; i++)
		{
			uint8_t send;
			send = output[i] + (i*10);
			my_printf("%d\n", send);
			BSP_DELAY_TIM_ms(2);
		}

		//my_printf("arrow = %d\r\n", output[2]);


		/*
		 if (dot == 0){
			my_printf("output = %d%d   , arrows = %d\r\n", digit1, digit2, arrow);
		}
		else{
			my_printf("output = %d.%d   , arrows = %d\r\n", digit1, digit2, arrow);
		}
		 */
		BSP_DELAY_TIM_ms(100);
	}
}


/*
 * helper functions
 */
uint8_t get_digit_1(uint32_t input)
{
	if((input & D1_msk) == D1_8) return 8;
	if((input & D1_msk) == D1_9) return 9;
	if((input & D1_msk) == D1_6) return 6;
	if((input & D1_msk) == D1_3) return 3;
	if((input & D1_msk) == D1_7) return 7;
	if((input & D1_msk) == D1_0) return 0;
	if((input & D1_msk) == D1_2) return 2;
	if((input & D1_msk) == D1_4) return 4;
	if((input & D1_msk) == D1_5) return 5;
	if((input & D1_msk) == D1_2) return 2;
	if((input & D1_msk) == D1_1) return 1;
	else return 99;	//error

}
uint8_t get_digit_2(uint32_t input)
{
	if((input & D2_msk) == D2_8) return 8;
	if((input & D2_msk) == D2_9) return 9;
	if((input & D2_msk) == D2_6) return 6;
	if((input & D2_msk) == D2_3) return 3;
	if((input & D2_msk) == D2_7) return 7;
	if((input & D2_msk) == D2_0) return 0;
	if((input & D2_msk) == D2_2) return 2;
	if((input & D2_msk) == D2_4) return 4;
	if((input & D2_msk) == D2_5) return 5;
	if((input & D2_msk) == D2_2) return 2;
	if((input & D2_msk) == D2_1) return 1;
	else return 99;	//error
}

uint8_t get_arrow(uint32_t input)
{
	if((input & ARR_1) == ARR_1) return 1;
	if((input & ARR_2) == ARR_2) return 2;
	if((input & ARR_3) == ARR_3) return 3;
	if((input & ARR_4) == ARR_4) return 4;
	if((input & ARR_5) == ARR_5) return 5;
	else return 0;	//no arrow
}
uint8_t get_dot(uint32_t input)
{
	if((input & DOT) == DOT) return 1;
	else return 0;	//no dot
}

void test_pins(uint32_t input)
{
	if((input & GPIO_A_0) == GPIO_A_0) my_printf("PA0   ");
	if((input & GPIO_A_1) == GPIO_A_1) my_printf("PA1   ");
	if((input & GPIO_A_4) == GPIO_A_4) my_printf("PA4   ");
	if((input & GPIO_A_5) == GPIO_A_5) my_printf("PA5   ");
	if((input & GPIO_A_6) == GPIO_A_6) my_printf("PA6   ");
	if((input & GPIO_A_7) == GPIO_A_7) my_printf("PA7   ");
	if((input & GPIO_A_8) == GPIO_A_8) my_printf("PA8   ");
	if((input & GPIO_A_9) == GPIO_A_9) my_printf("PA9   ");
	if((input & GPIO_A_10) == GPIO_A_10) my_printf("PA10   ");

	if((input & GPIO_C_0) == GPIO_C_0) my_printf("PC0   ");
	if((input & GPIO_C_1) == GPIO_C_1) my_printf("PC1   ");
	if((input & GPIO_C_2) == GPIO_C_2) my_printf("PC2   ");
	if((input & GPIO_C_3) == GPIO_C_3) my_printf("PC3   ");
	if((input & GPIO_C_4) == GPIO_C_4) my_printf("PC4   ");
	if((input & GPIO_C_5) == GPIO_C_5) my_printf("PC5   ");
	if((input & GPIO_C_6) == GPIO_C_6) my_printf("PC6   ");
	if((input & GPIO_C_7) == GPIO_C_7) my_printf("PC7   ");
	if((input & GPIO_C_10) == GPIO_C_10) my_printf("PC10   ");
	if((input & GPIO_C_11) == GPIO_C_11) my_printf("PC11   ");
	if((input & GPIO_C_12) == GPIO_C_12) my_printf("PC12   ");
	my_printf("\r\n");
}



/*
 * 	Clock configuration for the Nucleo STM32F072RB board
 * 	HSE input Bypass Mode 			-> 8MHz
 * 	SYSCLK, AHB, APB1 				-> 48MHz
 *  	PA8 as MCO with /16 prescaler 		-> 3MHz
 *
 *  Laurent Latorre - 05/08/2017
 */
static void SystemClock_Config()
{
	uint32_t	HSE_Status;
	uint32_t	PLL_Status;
	uint32_t	SW_Status;
	uint32_t	timeout = 0;

	timeout = 1000000;

	// Start HSE in Bypass Mode
	RCC->CR |= RCC_CR_HSEBYP;
	RCC->CR |= RCC_CR_HSEON;

	// Wait until HSE is ready
	do
	{
		HSE_Status = RCC->CR & RCC_CR_HSERDY_Msk;
		timeout--;
	} while ((HSE_Status == 0) && (timeout > 0));

	// Select HSE as PLL input source
	RCC->CFGR &= ~RCC_CFGR_PLLSRC_Msk;
	RCC->CFGR |= (0x02 <<RCC_CFGR_PLLSRC_Pos);

	// Set PLL PREDIV to /1
	RCC->CFGR2 = 0x00000000;

	// Set PLL MUL to x6
	RCC->CFGR &= ~RCC_CFGR_PLLMUL_Msk;
	RCC->CFGR |= (0x04 <<RCC_CFGR_PLLMUL_Pos);

	// Enable the main PLL
	RCC-> CR |= RCC_CR_PLLON;

	// Wait until PLL is ready
	do
	{
		PLL_Status = RCC->CR & RCC_CR_PLLRDY_Msk;
		timeout--;
	} while ((PLL_Status == 0) && (timeout > 0));

        // Set AHB prescaler to /1
	RCC->CFGR &= ~RCC_CFGR_HPRE_Msk;
	RCC->CFGR |= RCC_CFGR_HPRE_DIV1;

	//Set APB1 prescaler to /1
	RCC->CFGR &= ~RCC_CFGR_PPRE_Msk;
	RCC->CFGR |= RCC_CFGR_PPRE_DIV1;

	// Enable FLASH Prefetch Buffer and set Flash Latency
	FLASH->ACR = FLASH_ACR_PRFTBE | FLASH_ACR_LATENCY;

	/* --- Until this point, MCU was still clocked by HSI at 8MHz ---*/
	/* --- Switching to PLL at 48MHz Now!  Fasten your seat belt! ---*/

	// Select the main PLL as system clock source
	RCC->CFGR &= ~RCC_CFGR_SW;
	RCC->CFGR |= RCC_CFGR_SW_PLL;

	// Wait until PLL becomes main switch input
	do
	{
		SW_Status = (RCC->CFGR & RCC_CFGR_SWS_Msk);
		timeout--;
	} while ((SW_Status != RCC_CFGR_SWS_PLL) && (timeout > 0));

	/* --- Here we go! ---*/

	/*--- Use PA8 as MCO output at 48/16 = 3MHz ---*/

	// Set MCO source as SYSCLK (48MHz)
	RCC->CFGR &= ~RCC_CFGR_MCO_Msk;
	RCC->CFGR |=  RCC_CFGR_MCOSEL_SYSCLK;

	// Set MCO prescaler to /16 -> 3MHz
	RCC->CFGR &= ~RCC_CFGR_MCOPRE_Msk;
	RCC->CFGR |=  RCC_CFGR_MCOPRE_DIV16;

	// Enable GPIOA clock
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;

	// Configure PA8 as Alternate function
	GPIOA->MODER &= ~GPIO_MODER_MODER8_Msk;
	GPIOA->MODER |= (0x02 <<GPIO_MODER_MODER8_Pos);

	// Set to AF0 (MCO output)
	GPIOA->AFR[1] &= ~(0x0000000F);
	GPIOA->AFR[1] |=  (0x00000000);

	// Update SystemCoreClock global variable
	SystemCoreClockUpdate();
}
